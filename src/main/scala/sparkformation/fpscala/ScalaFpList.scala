package sparkformation.fpscala

object ScalaFpList {
  implicit class MyList[A](list: List[A]) {

    def myMap[B](f: A => B): List[B] = {
      list match {
        case Nil => ???
        case head :: tail => ???
      }
    }

    def myFilter(f: A => Boolean): List[A] = {
      list match {
        case Nil => ???
        case head :: tail => ???
      }
    }

    def myReduce[B](f: (A, A) => A): A = {
      list match {
        case Nil => throw new UnsupportedOperationException("empty.myReduce")
        case List(value) => value
        case first :: tail => ???
      }
    }

    def myFlatMap[B](f: A => List[B]): List[B] = {
      list match {
        case Nil => ???
        case head :: tail => ???
      }
    }

    // Optional
    def myFoldRight[B](z: B, f: (A, B) => B): B = {
      list match {
        case Nil => ???
        case head :: tail => ???
      }
    }

    // Optional
    def myReduceByFold(f: (A, A) => A): A = {
      ???
    }

  }
}