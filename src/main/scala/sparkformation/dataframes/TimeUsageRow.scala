package sparkformation.dataframes

case class TimeUsageRow(
                         working: String,
                         age: String,
                         primaryNeeds: Double,
                         work: Double,
                         other: Double
                       )
