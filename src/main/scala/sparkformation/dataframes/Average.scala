package sparkformation.dataframes

import org.apache.spark.sql.{Encoder, Encoders}
import org.apache.spark.sql.expressions.Aggregator

case class Average(sum: Int, count: Int)

object Average extends Aggregator[Int, Average, Double] {

  def zero: Average = {
    ???
  }

  def reduce(buffer: Average, data: Int): Average = {
    ???
  }

  def merge(b1: Average, b2: Average): Average = {
    ???
  }

  def finish(reduction: Average): Double = {
    ???
  }

  def bufferEncoder: Encoder[Average] = Encoders.product
  def outputEncoder: Encoder[Double] = Encoders.scalaDouble
}
