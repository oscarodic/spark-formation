package sparkformation.dataframes

import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.log4j.{Level, Logger}
object TimeUsage {
  import org.apache.spark.sql.SparkSession
  import org.apache.spark.sql.functions._

  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

  val primaryNeeds = List("t01", "t03", "t11", "t1801", "t1803")
  val workingActivities = List("t05", "t1805")
  val otherActivities = List("t02", "t04", "t06", "t07", "t08", "t09", "t10", "t12", "t13", "t14", "t15", "t16", "t18")

  val spark: SparkSession =
    SparkSession
      .builder()
      .appName("formation-dataframe")
      .master("local")
      .getOrCreate()

  import spark.implicits._

  def main(args: Array[String]): Unit = {
    val (columns, initDf) = read("src/main/resources/dataframes/time_usage.csv")
    val (primaryNeedsColumns, workColumns, otherColumns) = classifiedColumns(columns)
    val summaryDf = timeUsageSummary(primaryNeedsColumns, workColumns, otherColumns, initDf)
    val finalDf = timeUsageGrouped(summaryDf)
    finalDf.show()
    spark.close()
  }

  def read(path: String): (List[String], DataFrame) = {
    val df = spark.read.options(Map("header" -> "true", "inferSchema" -> "true")).csv(path)
    (df.schema.fields.map(_.name).toList, df)
  }

  def classifiedColumns(columnNames: List[String]): (List[Column], List[Column], List[Column]) =
    (
      columnNames.filter(columnName => primaryNeeds.exists(columnName.startsWith)).map(col),
      columnNames.filter(columnName => workingActivities.exists(columnName.startsWith)).map(col),
      columnNames.filter(columnName => otherActivities.exists(columnName.startsWith)).map(col)
    )

  def timeUsageSummary(
    primaryNeedsColumns: List[Column],
    workColumns: List[Column],
    otherColumns: List[Column],
    df: DataFrame
  ): DataFrame = {
    val workingStatusProjection: Column = ???
    val ageProjection: Column = ???

    val primaryNeedsProjection: Column = ???
    val workProjection: Column = ???
    val otherProjection: Column = ???
    df
      .select(workingStatusProjection, ageProjection, primaryNeedsProjection, workProjection, otherProjection)
      ???
  }

  def timeUsageGrouped(summed: DataFrame): DataFrame = {
    ???
  }

  def timeUsageGroupedSql(summed: DataFrame): DataFrame = {
    val viewName = s"summed"
    summed.createOrReplaceTempView(viewName)
    spark.sql(timeUsageGroupedSqlQuery(viewName))
  }

  def timeUsageGroupedSqlQuery(viewName: String): String = {
    ???
  }


  def timeUsageSummaryTyped(timeUsageSummaryDf: DataFrame): Dataset[TimeUsageRow] = {
    ???
  }

  def timeUsageGroupedTyped(summed: Dataset[TimeUsageRow]): Dataset[TimeUsageRow] = {
    ???
  }

  //val udfSum = ???

  def sumTimeSpendWithUdf(columns : List[Column]): Column = {
    ???
  }

  //val udafAvg = ???

  def avgWithUdaf(column: Column): Column = {
    ???
  }
}