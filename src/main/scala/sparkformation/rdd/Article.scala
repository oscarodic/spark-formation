package sparkformation.rdd

case class Article(title: String, text: String) {
  def mentionsLanguage(language: String): Boolean = text.split(' ').contains(language)
}
