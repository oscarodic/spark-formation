package sparkformation.rdd

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.log4j.{Logger, Level}

import org.apache.spark.rdd.RDD

object Ranking {
  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

  val languages = List(
    "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
    "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

  val conf: SparkConf = new SparkConf().setMaster("local").setAppName("formation-rdd")
  val sc: SparkContext = new SparkContext(conf)
  val articlesRdd: RDD[Article] = ???

  def occurrencesOfLanguage(language: String, rdd: RDD[Article]): Int = ???

  def rankLanguages(languages: List[String], rdd: RDD[Article]): List[(String, Int)] = ???

  def makeIndex(languages: List[String], rdd: RDD[Article]): RDD[(String, Iterable[Article])] = ???

  def rankLanguagesUsingIndex(index: RDD[(String, Iterable[Article])]): List[(String, Int)] = ???

  def rankLanguagesReduceByKey(languages: List[String], rdd: RDD[Article]): List[(String, Int)] = ???

  def main(args: Array[String]): Unit = {

    println("Part 1 :")
    val beginPart1 = System.currentTimeMillis()
    val languagesRanked: List[(String, Int)] = rankLanguages(languages, articlesRdd)
    val endPart1 = System.currentTimeMillis()
    printLanguagesRanked(languagesRanked)
    println(s"${endPart1 - beginPart1} ms")

    println("=========================")

    def index: RDD[(String, Iterable[Article])] = makeIndex(languages, articlesRdd)

    println("Part 2 :")
    val beginPart2 = System.currentTimeMillis()
    val languagesRanked2: List[(String, Int)] = rankLanguagesUsingIndex(index)
    val endPart2 = System.currentTimeMillis()
    printLanguagesRanked(languagesRanked2)
    println(s"${endPart2 - beginPart2} ms")

    println("=========================")

    println("Part 3 :")
    val beginPart3 = System.currentTimeMillis()
    val languagesRanked3: List[(String, Int)] = rankLanguagesReduceByKey(languages, articlesRdd)
    val endPart3 = System.currentTimeMillis()
    printLanguagesRanked(languagesRanked3)
    println(s"${endPart3 - beginPart3} ms")

    sc.stop()
  }

  def printLanguagesRanked(languagesRanked: List[(String, Int)]): Unit =
    languagesRanked
      .zipWithIndex
      .foreach {
        case ((language, n), index) => println(s"${index + 1}- $language ($n)")
      }
}
