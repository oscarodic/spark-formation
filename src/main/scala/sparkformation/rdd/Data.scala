package sparkformation.rdd

import scala.io.Source
import scala.io.Codec
import org.apache.spark.sql.functions

object Data {
  def lines: List[String] =
    Option(getClass.getResourceAsStream("/rdd/articles.txt")) match {
      case None => sys.error("Please download the dataset as explained in the assignment instructions")
      case Some(resource) => Source.fromInputStream(resource)(Codec.UTF8).getLines().toList
    }

  def parse(line: String): Article = {
    val subs = "</title><text>"
    val i = line.indexOf(subs)
    val title = line.substring(14, i)
    val text  = line.substring(i + subs.length, line.length-16)
    Article(title, text)
  }
}
