package sparkformation.rdd

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class TestRanking extends AnyFlatSpec with should.Matchers {
  import Ranking._

  "occurrencesOfLanguage" should "work for (specific) RDD with one element" in {
    val rdd = sc.parallelize(Seq(Article("title", "Java Jakarta")))
    occurrencesOfLanguage("Java", rdd) should be(1)
  }

  "rankLanguages" should "work for RDD with two elements" in {
    val languages = List("Scala", "Java")
    val rdd = sc.parallelize(List(Article("1", "Scala is great"), Article("2", "Java is OK, but Scala is cooler")))
    val expectedRank = List(
      ("Scala",2),
      ("Java",1)
    )
    val ranked = rankLanguages(languages, rdd)
    ranked should be(expectedRank)
  }

  "makeIndex" should "creates a simple index with two entries" in {
    val languages = List("Scala", "Java")
    val articles = List(
      Article("1","Groovy is pretty interesting, and so is Erlang"),
      Article("2","Scala and Java run on the JVM"),
      Article("3","Scala is not purely functional")
    )
    val rdd = sc.parallelize(articles)
    val expectedIndexes = List(
      ("Java", Seq(
        Article("2","Scala and Java run on the JVM")
      )),
      ("Scala",Seq(
        Article("2","Scala and Java run on the JVM"),
        Article("3","Scala is not purely functional")
      )))

    val index = makeIndex(languages, rdd)
    index.sortBy(_._1).collect().toList should be(expectedIndexes)
  }

  "rankLanguagesUsingIndex" should "work for a simple RDD with three elements" in {
    val languages = List("Scala", "Java")
    val articles = List(
      Article("1","Groovy is pretty interesting, and so is Erlang"),
      Article("2","Scala and Java run on the JVM"),
      Article("3","Scala is not purely functional")
    )
    val rdd = sc.parallelize(articles)
    val expectedRanks = List(
      ("Scala",2),
      ("Java",1)
    )
    val index = makeIndex(languages, rdd)
    val ranked = rankLanguagesUsingIndex(index)
    ranked should be(expectedRanks)
  }

  "rankLanguagesReduceByKey" should "work for a simple RDD with five elements" in {
    val languages = List("Scala", "Java", "Groovy", "Haskell", "Erlang")
    val articles = List(
      Article("1","Groovy is pretty interesting, and so is Erlang"),
      Article("2","Scala and Java run on the JVM"),
      Article("3","Scala is not purely functional"),
      Article("4","The cool kids like Haskell more than Java"),
      Article("5","Java is for enterprise developers")
    )
    val rdd = sc.parallelize(articles)
    val expectedRanks = List(
      ("Java",3),
      ("Scala",2),
      ("Haskell",1),
      ("Groovy",1),
      ("Erlang",1)
    )
    val ranked = rankLanguagesReduceByKey(languages, rdd)
    ranked should be(expectedRanks)
  }
}
