package sparkformation.fpscala
import org.scalatest._
import flatspec._
import matchers._
import ScalaFpList.MyList

class TestScalaFpList extends AnyFlatSpec with should.Matchers {
  "list.myMap" should "be correct" in {
    List(1, 2, 3, 4, 5).myMap(x => x * 2) should be(List(2, 4, 6, 8, 10))
    List.empty[Int].myMap(x => x * 2) should be(List.empty[Int])
  }

  "list.myFilter" should "be correct" in {
    List(1, 4, 3, 5, 2).myFilter(x => x > 3) should be(List(4, 5))
    List.empty[Int].myFilter(x => x > 3) should be(List.empty[Int])
  }

  "list.myReduce" should "be correct" in {
    List(1, 2, 3, 4, 5).myReduce((x, y) => x + y) should be(15)
  }

  "list.myFlatMap" should "be correct" in {
    List(1, 2, 3, 4, 5).myFlatMap(x => List(x, x*x)) should be(List(1, 1, 2, 4, 3, 9, 4, 16, 5, 25))
  }

  "list.myFoldRight" should "be correct" in {
    List(1, 2, 3, 4, 5).myFoldRight[String]("", (x, y) => x.toString + y) should be("12345")
  }

  "list.myReduceByFold" should "be correct" in {
    List(1, 2, 3, 4, 5).myReduce((x, y) => x + y) should be(15)
  }
}
